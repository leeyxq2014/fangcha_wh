#!/bin/bash
##################################################################################################################
#1、仅适用于江月府备案
#2、若其他楼盘，请修改参数：xiangMmc[项目名称：新建居住项目（青菱村城中村改造K3地块）]、domain[区域：洪山区]
#3、添加至crontab
#   cp fangcha.sh /opt/ && cp mail.py /opt/
#	crontab -e
#   0 0 8/4 * * /bin/sh /opt/fangcha.sh >/opt/fangcha.log
##################################################################################################################


#备案和楼盘信息
#备案号， 如洪121212
dengjh=洪121212
#区域 如：洪山区
domain=洪山区
#项目名称 如：新建居住项目（青菱村城中村改造K3地块）
xiangMmc=新建居住项目（青菱村城中村改造K3地块）
#身份证号 如:411232323232323
renybh=411232323232323

#邮件账户信息
smtp=smtp.126.com
username=test
password=123456
mail_sender=test@126.com
mail_receiver=test@126.com

current_dir=$(cd `dirname $0`; pwd)


#1、抓取备案信息
result=$(curl -s 'http://fgj.wuhan.gov.cn/zz_grxxcx_search.jspx' \
-XPOST \
-H 'Content-Type: application/x-www-form-urlencoded' \
-H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8' \
-H 'Host: fgj.wuhan.gov.cn' \
-H 'Accept-Language: zh-cn' \
-H 'Accept-Encoding: gzip, deflate' \
-H 'Origin: http://fgj.wuhan.gov.cn' \
-H 'Referer: http://fgj.wuhan.gov.cn/zz_grxxcx_search.jspx' \
-H 'Upgrade-Insecure-Requests: 1' \
-H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1 Safari/605.1.15' \
-H 'Connection: keep-alive' \
--data-urlencode dengjh="${dengjh}" \
--data-urlencode renybh="${renybh}" \
--data-urlencode domain="${domain}" \
--data-urlencode xiangMmc="${xiangMmc}" \
|grep '<td id="td_htzt">'|awk -F'[<>]' '{print $3}')

echo "curl ret2: ${result}"

#2、判断和邮件通知

#已备案通知
#if [[ "$result" == "已备案"  ]]; then

#已签约通知
#if [[ "$result" == "已签约"  ]]; then

#一直每天通知
if [[ -n "$result"  ]]; then
	echo 'send mail begin'
	python ${current_dir}/mail.py ${smtp} ${mail_sender} ${mail_receiver} "备案状态已改变" ${username} ${password} "${result}" 
	echo 'send mail end'
fi
