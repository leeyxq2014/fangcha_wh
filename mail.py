#!/usr/bin/python
# -*- coding: UTF-8 -*-
import smtplib, sys
from email.mime.text import MIMEText
from email.header import Header
from sys import argv 
 
if len(argv) == 8:
    smtp,sender,recipients,subject,username,password,mail_content=argv[1:]
    recipients = recipients.split(",")
else:
    print "Usage: %s smtp,sender,recipients,subject,username,password,mail_content" %(argv[0])
    exit()
 
message = MIMEText(mail_content, 'plain', 'utf-8')


message['Subject'] = Header(subject, 'utf-8')

smtpObj = smtplib.SMTP() 
smtpObj.connect(smtp, 25)    # 25 为 SMTP 端口号
smtpObj.login(username,password)  
smtpObj.sendmail(sender, recipients, message.as_string())
smtpObj.quit()
print "邮件发送成功"
