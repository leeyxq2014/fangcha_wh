fangcha_wh

#### 介绍
用于武汉备案状态自动通知

#### 安装教程

1. 下载程序


```
curl -o fangcha.zip https://gitee.com/leeyxq2014/fangcha_wh/repository/archive/master.zip 

```

2. 复制文件至/opt目录

```
cp fangcha.sh /opt/ && cp mail.py /opt/

```

3. 添加定时任务

```
crontab -e
31 15 * * * /bin/sh /opt/fangcha.sh >/opt/fangcha.log
```


#### 使用说明

1. 目前仅运行centos7上，不确保其他系统是否正常
2. 仅适用于江月府备案，若其他楼盘，请修改参数：xiangMmc[项目名称：新建居住项目（青菱村城中村改造K3地块）]、domain[区域：洪山区]
3. 请修改fangcha.sh文件中的备案和邮件信息
4.  Enjoy it!

#### 需修改部分

![马赛克需修改部分](https://images.gitee.com/uploads/images/2019/0509/162629_2d32a624_617236.png "WX20190509-162611@2x.png")

#### 效果图

![邮件通知效果](https://images.gitee.com/uploads/images/2019/0509/162319_21aba8bc_617236.png "1F8B22D6-9458-4970-B6F9-8EA949BFB77A.png")
